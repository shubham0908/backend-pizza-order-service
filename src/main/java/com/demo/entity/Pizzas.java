package com.demo.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Pizzas {

    private Integer pizzaId;

    @NotBlank(message = "Pizza name is mandatory")
    private String name;

    @NotBlank(message = "Pizza description is mandatory")
    private String description;

    @NotBlank(message = "Pizza type is mandatory")
    private String type;

    @NotBlank(message = "Image url is mandatory")
    private String imageUrl;


    @NotNull(message = "price is mandatory")
    private Integer priceRegularSize;

    @NotNull(message = "price is mandatory")
    private Integer priceMediumSize;

    @NotNull(message = "price is mandatory")
    private Integer priceLargeSize;

    private Integer quantity;

    private String size;

    private List<OrderLine> orderLine;

    private Integer price;




}
