package com.demo.mapper;

import com.demo.entity.OrderLine;
import com.demo.entity.Orders;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OrderLineMapper {

    public Integer createOrderLine(@Param("orderLine")OrderLine orderLine);
    List<OrderLine> getAllOrderLines();

    List<OrderLine> getOrderLinesByOrderId(Integer orderId);

    void updateOrderLine(OrderLine orderLine);
}
