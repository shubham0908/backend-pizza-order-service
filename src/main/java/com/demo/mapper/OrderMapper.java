package com.demo.mapper;

import com.demo.entity.Orders;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OrderMapper {
    Integer  createOrder(@Param("orders") Orders orders);

    List<Orders> getAllOrders();

    void deleteOrder(Integer orderId);

    void updateOrder(Orders updatedOrder);
}
