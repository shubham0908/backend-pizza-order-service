package com.demo.response;

import com.demo.entity.Customer;
import org.springframework.http.ResponseEntity;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ResponseUtil {


    public static ResponseEntity<Object> successResponse(boolean success, String message , Object data) {
        Map<String, Object> response = new LinkedHashMap<>();

        response.put("Success",true);
        response.put("Message", message);
        response.put("Data", data);
        return ResponseEntity.ok(response);

    }

    public static ResponseEntity<Object>successResponse(boolean Success, String message, List<Object> list){
        LinkedHashMap<String,Object>response=new LinkedHashMap<>();
        response.put("Success",true);
        response.put("Message",message);
        response.put("Data",list);
        return ResponseEntity.ok(response);
    }

    public static ResponseEntity<Object>errorResponse(boolean Success,String message,Object error )
    {
        LinkedHashMap<String,Object>response=new LinkedHashMap<>();
        response.put("Success",false);
        response.put("Message",message);
        response.put("Error",error);
        return ResponseEntity.ok(response);
    }

    public static ResponseEntity<Object>errorResponse(boolean Success,String message,List<Object> error )
    {
        LinkedHashMap<String,Object>response=new LinkedHashMap<>();
        response.put("Success",false);
        response.put("Message",message);
        response.put("Error",error);
        return ResponseEntity.ok(response);
    }

}
