package com.demo.controller;

import com.demo.dto.OrderResponseList;
import com.demo.entity.Orders;
import com.demo.dto.OrderResponse;
import com.demo.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class OrderController {

   @Autowired
   OrderService orderService;


    @PostMapping("/orders")
    @CrossOrigin(origins = "http://localhost:3000")
    public ResponseEntity<OrderResponse> createOrder(@RequestBody Orders orders) {

            OrderResponse orderResponse= orderService.createOrder(orders);

            return  new ResponseEntity<>(orderResponse, HttpStatus.CREATED);


    }

    @GetMapping("/orders")
    @CrossOrigin(origins = "http://localhost:3000")
    public ResponseEntity<OrderResponseList> getAllOrders() {
       // log.info("inside getAllOrders method of Order Controller");

        OrderResponseList orderResponse=  orderService.getAllOrders();

        return new ResponseEntity<>(orderResponse, HttpStatus.OK);
    }


    @DeleteMapping("/orders/{orderId}")
    public String deleteOrder(@PathVariable Integer orderId){
        orderService.deleteOrder(orderId);
        return "order deleted successfully";
    }


    @PutMapping("/orders/{orderId}")
    @CrossOrigin(origins = "http://localhost:3000")
    public ResponseEntity<OrderResponse> updateOrder(@PathVariable Integer orderId, @RequestBody Orders orders) {

        OrderResponse orderResponse= orderService.updateOrder(orders);

        return  new ResponseEntity<>(orderResponse, HttpStatus.CREATED);


    }


}
