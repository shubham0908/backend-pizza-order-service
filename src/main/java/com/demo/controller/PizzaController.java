package com.demo.controller;

import com.demo.entity.Pizzas;
import com.demo.exception.ErrorMessageResponse;
import com.demo.response.ResponseUtil;
import com.demo.service.PizzaService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
public class PizzaController {

    @Autowired
    private PizzaService pizzaService;


    @PostMapping("/pizzas")
    public ResponseEntity<Object> createPizzas (@Valid @RequestBody Pizzas pizza) {
        log.info("Inside createPizzas handler");
        pizzaService.createPizzas(pizza);
        log.info("Successfully created pizza");
        return ResponseUtil.successResponse(true ,"successfully created pizza",pizza);

    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/pizzas/{id}")
    public ResponseEntity<Object> getPizzaDetails(@PathVariable("id") Integer pizzaId) {
        log.info("Inside getPizzaDetails handler");
        Pizzas pizza= pizzaService.getPizzaDetails(pizzaId);

        if(pizza==null)
        {
         ErrorMessageResponse response=ErrorMessageResponse.builder().messaage("Not found").code(400).build();
         log.info("failed to fetched pizza details");
         return  ResponseUtil.errorResponse(false,"failed to fetched pizza details",response);
        }
        else {
            log.info("successfully fetched pizza detail");
        return ResponseUtil.successResponse(true, "successfully fetched pizza detail", pizza);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/pizzas")
    public ResponseEntity<Object> getAllPizzas() {
        log.info("Inside get all pizzas");
        List<Pizzas> pizzas= pizzaService.getAllPizzas();

        if(pizzas==null)
        {
         ErrorMessageResponse response=ErrorMessageResponse.builder().messaage("Not found").code(400).build();
            log.info("failed to fetched pizza details");
            return  ResponseUtil.errorResponse(false,"failed to fetched all pizza details",response);
        }else {
            log.info("successfully fetched all pizza");
            return ResponseUtil.successResponse(true, "successfully fetched all pizza details", pizzas);
        }
    }

    @PutMapping("/pizzas/{pizzaId}")
    public ResponseEntity<Object> updatePizzaDetails(@PathVariable int pizzaId,@RequestBody Pizzas pizza){
        log.info("Inside update pizza details handler");
        pizza.setPizzaId(pizzaId);
         pizzaService.updatePizzaDetails(pizza);
        log.info("Successfully updated pizza details");
        return ResponseUtil.successResponse(true ,"successfully updated pizza detail",pizza);

    }

    @DeleteMapping("/pizzas/{id}")
    public ResponseEntity<Object> deletePizza(@PathVariable("id") Integer id) {
      log.info("Inside deletePizza handler");
      boolean response=  pizzaService.deletePizza(id);
      if(!response)
      {
          ErrorMessageResponse errorMessageResponse=ErrorMessageResponse.builder().code(404).messaage("Invalid Pizza ID").build();
          log.info("failed to delete pizza");
          return  ResponseUtil.errorResponse(false,"falied to delete pizza",errorMessageResponse);
      }
      else
      {
          log.info("successfully deleted pizza");
          return  ResponseUtil.successResponse(true,"successfully deleted pizza","Deleted pizza id is"+id);
      }


    }




}
