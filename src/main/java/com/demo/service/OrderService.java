package com.demo.service;

import com.demo.dto.OrderResponseList;
import com.demo.entity.OrderLine;
import com.demo.entity.Orders;
import com.demo.entity.Pizzas;
import com.demo.mapper.OrderLineMapper;
import com.demo.mapper.OrderMapper;
import com.demo.dto.OrderResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class OrderService {

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private OrderLineMapper orderLineMapper;





    public OrderResponse createOrder (Orders order) {





        Orders createdOrder = Orders.builder().orderDateTime(String.valueOf(new Date())).customerId(order.getCustomerId()).status("Pending").totalAmount(order.getTotalAmount()).deliveryAddress(order.getDeliveryAddress()).orderId(order.getOrderId()).build();


        Integer orderId = orderMapper.createOrder(createdOrder);


        for (Pizzas pizza : order.getPizzas()) {

            OrderLine orderLine = new OrderLine();
            orderLine.setOrderId(createdOrder.getOrderId());
            orderLine.setPizzaId(pizza.getPizzaId());
            orderLine.setQuantity(pizza.getQuantity());
            orderLine.setSize(pizza.getSize());
            orderLine.setTotalPrice(pizza.getPrice());


            orderLineMapper.createOrderLine(orderLine);
        }

        OrderResponse orderResponse = OrderResponse.builder().success(true).message("Order Created SuccesFully").order(createdOrder).build();

        return orderResponse;

    }

    public  OrderResponseList getAllOrders() {
        List<Orders> multipleOrders = orderMapper.getAllOrders();
        List<Orders> orderList = new ArrayList<>();

        for (Orders orders : multipleOrders) {
            Orders order = new Orders();
            order.setOrderId(orders.getOrderId());
            order.setCustomerId(orders.getCustomerId());
            order.setDeliveryAddress(orders.getDeliveryAddress());
            order.setStatus(orders.getStatus());
            order.setTotalAmount(orders.getTotalAmount());
            order.setOrderDateTime(orders.getOrderDateTime());

            List<OrderLine> orderLines = orderLineMapper.getOrderLinesByOrderId(orders.getOrderId());
            List<Pizzas> pizzas = new ArrayList<>();

            for (OrderLine orderLine : orderLines) {
                Pizzas pizza = new Pizzas();
                pizza.setPizzaId(orderLine.getPizzaId());
                pizza.setSize(orderLine.getSize());
                pizza.setQuantity(orderLine.getQuantity());
               pizza.setPrice(orderLine.getTotalPrice());
                pizzas.add(pizza);
            }

            order.setPizzas(pizzas);
            orderList.add(order);
        }
        OrderResponseList orderResponse = (OrderResponseList) OrderResponseList.builder().success(true).message("Successfully fetch all orders").Data(orderList).build();

        return orderResponse;

    }

    public void deleteOrder(Integer orderId) {
        orderMapper.deleteOrder(orderId);
    }


    public OrderResponse updateOrder(Orders order) {
    //  Orders updatedOrder = Orders.builder().status("Pending").totalAmount(order.getTotalAmount()).deliveryAddress(order.getDeliveryAddress()).orderId(order.getOrderId()).build();
        orderMapper.updateOrder(order);
        for (Pizzas pizza : order.getPizzas()) {
            OrderLine orderLine = new OrderLine();
            orderLine.setOrderId(order.getOrderId());
            orderLine.setPizzaId(pizza.getPizzaId());
            orderLine.setQuantity(pizza.getQuantity());
            orderLine.setSize(pizza.getSize());
            orderLine.setTotalPrice(pizza.getPrice());
            orderLineMapper.updateOrderLine(orderLine);
        }
        OrderResponse orderResponse = OrderResponse.builder().success(true).message("Order updated Succesfully").order(order).build();
        return orderResponse;
    }
}
