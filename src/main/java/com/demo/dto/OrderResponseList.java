package com.demo.dto;

import com.demo.entity.OrderLine;
import com.demo.entity.Orders;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderResponseList {

    Boolean success ;
    String message;

   List <Orders> Data;

}
