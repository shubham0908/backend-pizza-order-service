package com.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PizzaResponse {

    private Integer pizzaId;
    private String size;
    private Integer quantity;
    private Integer subTotal;
}
